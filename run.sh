#!/bin/sh

$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix advection_dispersion
python advection_dispersion_numerical.py
python advection_dispersion_analytical.py
python plot.py &
