#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 19:34:06 2020

@author: gehammo
"""

import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse
import matplotlib.pyplot as plt

n = 1000

matrix = np.zeros((n,n),dtype=float)
rhs = np.zeros(n,dtype=float)
conc = np.zeros(n,dtype=float)
conc[:] = 1.e-10
concbc = 1.e-3

yr_to_sec = 3600.*24.*365.

u = 1./yr_to_sec
# Peter
D = 1.e-7  # diffusion coefficient in m^2/s

por = 1.
dx = 100./n
dy = 1.
dz = 1.
V = dx*dy*dz

dt = dx/u
nts = int(n/2)
print(nts)
print(dt)
print(dx)

A = dy*dz

accum = por*V/dt
adv = u*A
dif = D*A/dx

for i in range(n):
    matrix[i,i] += accum + adv + 2.*dif
    if i > 0:
        matrix[i,i-1] += -adv - dif
    if i < n-1:
        matrix[i,i+1] += -dif
    else: # zero gradient
        matrix[i,i] -= dif
    
A = sparse.csr_matrix(matrix)
#print(A)
for i in range(nts):
 #   print(i)
    rhs[:] = accum*conc[:]
    rhs[0] += adv*concbc + dif*concbc
   # rhs[n-1] += dif*conc[n-1]
    conc = spsolve(A,rhs)
    
#print(A)
#print(0,conc[0])
#print(25,conc[int(0.25*n)])
#print('49.x',conc[int(n/2-1)])
#print(50,conc[int(0.5*n)])
#print(75,conc[int(0.75*n)])
#print(100,conc[-1])
x = np.arange(dx/2,n*dx+dx/2,dx,dtype=float)
#plt.plot(x,conc)
#plt.show()

f = open('advection_dispersion_numerical.results','w')
f.write('"Distance [m]", "Concentration [M]"\n'.format(x[i],conc[i]))
for i in range(n):
    f.write('{} {}\n'.format(x[i],conc[i]))
f.close()
