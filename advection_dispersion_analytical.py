import sys
import traceback

#sys.path.insert(0,'..')
sys.path.insert(0,'/home/gehammo/software/qa-toolbox')

from qa_solution import QASolutionWriter
from simulator_modules.python import *
from analytical_modules.javandel import Javandel

def main(options):

    javandel = Javandel()

    t = 50. # y
    tunit = 'y'
    sec_per_yr = 3600.*24.*365.

    # Peter
    D = 1.e-7*sec_per_yr # sec_per_yr converts m^2/s -> m^2/y for script
    pore_water_velocity = 1. # m/y

    javandel.set_Lx(100.)
    javandel.set_nx(1000)
    javandel.set_diffusion(D)
    javandel.set_velocity(pore_water_velocity)
    javandel.set_c0(1.e-3)
    x, c = javandel.get_solution(t)
    x, y, z = javandel.get_cell_centered_coordinates()
    solution_filename = get_python_solution_filename(__file__)
    solution = QASolutionWriter(solution_filename,tunit)
    solution.write_coordinates(x,y,z)
    solution.write_dataset(t,c,'Tracer')
    solution.destroy()
    for i in range(len(c)):
      print('{} {}'.format(i+0.5,c[i]))

if __name__ == "__main__":
  cmdl_options = ''
  try:
    suite_status = main(cmdl_options)
    print("success")
    sys.exit(suite_status)
  except Exception as error:
    print(str(error))
#    if cmdl_options.backtrace:
#      traceback.print_exc()
    traceback.print_exc()
    print("failure")
    sys.exit(1)

